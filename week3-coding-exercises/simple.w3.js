//to check the code, uncomment the corresponding question.

question1();
question2();
question3();

function question1(){

    var outputAreaRef = document.getElementById("outputArea1");
    var output = "";

    function flexible(fOperation, operand1, operand2){
        var result = fOperation(operand1, operand2);
        return result;
    }
    function additionFunc(num1, num2) {
        return num1 + num2
    }
    function multiplicationFunc(num1, num2) {
        return num1 * num2
    }
    
    output += flexible( additionFunc, 3, 5) + "<br/>";
    output += flexible( multiplicationFunc, 3, 5) + "<br/>"; 
    outputAreaRef.innerHTML = output;

    outputAreaRef.innerHTML = output;
  

}

function question2(){
    let output = "" 
    
    var testObj = {
        number: 1,
        string: "abc",
        array: [5, 4, 3, 2, 1],
        boolean: true
    };
    
    var testObj2 = {
        name: "Bob",
        age: 32, 
        married: true,
        children: ["Jack", "Jill"]
    };
    
    function printObj(objectName){
        
        var objProp=Object.keys(objectName);
        var objToString ="";
        for(var objkey of objProp){
            
            objToString += objkey + ": " + objectName[objkey] + "\n"; 
        }
        return objToString;
  
    }

    output += printObj(testObj)
    
    output += "\n"
    
    output += printObj(testObj2)
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    var values = [4, 3, 6, 12, 1, 3, 8];

function extremeValues(myArray){
   
   var min=myArray[0];
   var max=myArray[0];
   
   for (i=0; i<myArray.length; i++){
     if (myArray[i]< min){
       min = myArray[i];
     }
   }
    for (i=0; i<myArray.length; i++){
         if (myArray[i]>max){
           max = myArray[i];
         }
   }
  
    let minMaxObj= {
        minimum:min,
        maximum:max
    }  
    return minMaxObj
    }
    let printObj = (extremeValues(values)) 
    
    output += printObj.maximum + "\n"
    
    output += printObj.minimum
    
    let outPutArea = document.getElementById("outputArea3") 
    outPutArea.innerText = output 
  
  
}

