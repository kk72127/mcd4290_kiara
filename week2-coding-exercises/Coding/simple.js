//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){   //Question 1
    let output = "" //empty output, fill this so that it can print onto the page.
    
    let arrayQ=[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, 
-51, -17, -25];


    let posOdd =[];
    let negEven =[];

    for (var i =0; i<arrayQ.length;i++ ){
        if ((arrayQ[i] % 2 ==1) && (arrayQ[i]>0)){
            posOdd.push(arrayQ[i]);
        }
    else if ((arrayQ[i] % 2==0) && (arrayQ[i]<0)){
        negEven.push(arrayQ[i]);       
        }
    }

    output += "Positive Odd: ";
    for (var i =0; i<posOdd.length;i++ ){
        output += posOdd[i];
        if (posOdd.length!= i+1){
            output += ", ";
        }
    }

    output += "\n";
    output += "Negative Even : ";

    for (var i =0; i<negEven.length;i++ ){
        output += negEven[i];
        if (negEven.length!= i+1){
            output += ", ";
        }
    } 
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}




function question2(){  //Question 2 Part A
    let output = "" 
    
    
   let countArr = [0, 0, 0, 0, 0, 0];
    for (var i=0; i<60001; i++){
        var dice = Math.floor((Math.random() * 6) + 1);
        if (dice == 1){
            countArr[0] +=1;
        }
        else if (dice == 2){
            countArr[1] +=1;
        }
        else if (dice == 3){
            countArr[2] +=1;
        }
        else if (dice == 4){
            countArr[3] +=1;
        }
        else if (dice == 5){
            countArr[4] +=1;
        }
        else if (dice == 6){
            countArr[5] +=1;
        }
    }
output = "Frequency of Die Rolls:"+ "\n"+ "1: "+countArr[0]+"\n"+ "2: "+countArr[1]+"\n"+ "3: "+countArr[2]+"\n"+ "4: "+countArr[3]+"\n"+ "5: "+countArr[4]+"\n"+ "6: "+countArr[5];
 

    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}




function question3(){  //Question 2 Part B
    let output = "" 
    
     
    let countArr2 = [0, 0, 0, 0, 0, 0,0];
    for (var i=0; i<60001; i++){
        var dice = Math.floor((Math.random() * 6) + 1);
        if (dice == 1){
            countArr2[1] +=1;
        }
        else if (dice == 2){
            countArr2[2] +=1;
        }
        else if (dice == 3){
            countArr2[3] +=1;
        }
        else if (dice == 4){
            countArr2[4] +=1;
        }
        else if (dice == 5){
            countArr2[5] +=1;
        }
        else if (dice == 6){
            countArr2[6] +=1;
        }
  
    }
    
    output = "Frequency of Die Rolls:"+ "\n"+ "1: "+countArr2[1]+"\n"+ "2: "+countArr2[2]+"\n"+ "3: "+countArr2[3]+"\n"+ "4: "+countArr2[4]+"\n"+ "5: "+countArr2[5]+"\n"+ "6: "+countArr2[6];
    
   
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}




function question4(){  //Question 2 Part C
    let output = "" 
    
    //Question 4 here 
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}




function question5(){  // Question 3
    let output = "" 
    
     let person = {
        name: "Jane",
        income: 127050
    }
    var taxVal;

    if (person.income<=18200){
        taxVal =0;
    }
    else if ((person.income>= 18201) && (person.income<=37000)){
        taxVal = (person.income - 18200)*0.19;
    }
    else if ((person.income>=37001) && (person.income<=90000)){
        taxVal = (person.income -37000)*32.5 +3572;
    }
    else if ((person.income>=90001) && (person.income<=180000)){
        taxVal = (person.income-90000)*0.37 + 20797;
    }
    else if (person.income>=180001){
        taxVal = (person.income-180000)*0.45 + 54097;
    }

    output = person.name +"'s income is: $" + person.income + ", and her tax owed is: $"+taxVal.toFixed(2);
     
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}