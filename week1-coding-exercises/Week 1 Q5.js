/* original
let year;
let yearNot2015Or2016;

year = 2000;

yearNot2015Or2016 = year !== 2015 || year !== 2016;

console.log(yearNot2015Or2016);
no matter what the value of year is, it will always return true according to the truth tables

*/


//fixed

let year;
let yearNot2015And2016;

year = 2000;

yearNot2015Or2016 = year !== 2015 && year !== 2016;

console.log(yearNot2015Or2016);
// now if the value of year is 2015 0r 2016 it will return false