const person = {
  firstName: "Kanye",
  lastName: "West",
  birthDate: "8 June 1977",
  annualIncome: "150000000.00"
};

console.log(person.firstName + " " + person.lastName + " was born on " + person.birthDate + " and has an annual income of $" + parseInt(person.annualIncome));